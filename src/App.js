import React from 'react';
import './App.css';
import { Table, Divider, Tag, Icon } from 'antd';
import moment from 'moment'
import AddItem from './AddItem'
import Button from 'antd/lib/button';

const dateFormat = 'YYYY-MM-DD';

class App extends React.Component {
    state = {
        filteredInfo: null,
        visible: false,
        newItem: {},
        isAllset: true,
        name: null,
        dueDate: moment('2015-06-06', dateFormat).format('YYYY-MM-DD'),
        numberOfResources: null,
        status: null,
        items: [],
        selectedItem: null,
        editVisible: false,
        key: null,

    };

    componentDidMount = () => {
        if (localStorage.getItem('items')) {
            this.setState({
                items: JSON.parse(localStorage.getItem('items'))
            })
        }
    }

    onOptionChange = (value) => {
        this.setState({
            status: value
        })
    }

    showModal = () => {
        this.setState({
            visible: true,
        });
    };

    handleDelete = (key) => {
        let filteredItem = this.state.items.filter((value, index) => {
            return value.key !== key
        })
        this.setState({
            items: filteredItem
        })
    }

    handleEdit = (key) => {
        let filteredItem = this.state.items.filter((value, index) => {
            return value.key === key
        })
        this.setState({
            selectedItem: filteredItem[0],
            name: filteredItem[0].name,
            dueDate: filteredItem[0].dueDate,
            numberOfResources: filteredItem[0].numberOfResources,
            key: filteredItem[0].key,
            status: filteredItem[0].status,
            editVisible: true
        })
    }

    handleEditOk = () => {
        let filteredItems = this.state.items.map((value, index) => {
            if (value.key === this.state.key) {
                let newItem = {}
                newItem.name = this.state.name
                newItem.dueDate = this.state.dueDate
                newItem.numberOfResources = this.state.numberOfResources
                newItem.key = this.state.key
                newItem.status = this.state.status
                return newItem
            } else {
                return value
            }
        })
        localStorage.setItem('items', JSON.stringify(filteredItems))
        this.setState({
            items: filteredItems,
            editVisible: false,
            name: null,
            numberOfResources: null,
            status: null
        })
    }
    handleOk = e => {
        let newItem = {}
        let items = this.state.items
        newItem.name = this.state.name
        newItem.dueDate = this.state.dueDate
        newItem.numberOfResources = this.state.numberOfResources
        newItem.status = this.state.status
        newItem.key = this.state.items.length + 1
        items.push(newItem)
        localStorage.setItem('items', JSON.stringify(items))
        this.setState({
            visible: false,
            items: items,
            name: null,
            numberOfResources: null,
        });
    };

    handleCancel = e => {
        this.setState({
            visible: false,
            name: null,
            numberOfResources: null,
            status: null
        });
    };

    handleEditCancel = e => {
        this.setState({
            editVisible: false,
            name: null,
            numberOfResources: null,
            status: null
        });
    };

    onHandleDate = (date, dateString) => {
        this.setState({
            dueDate: dateString
        })
    }

    onHandleName = e => {
        this.setState({
            name: e.target.value
        })
    }

    onHandleAge = e => {
        this.setState({
            numberOfResources: e
        })
    }


    render() {
        let { filteredInfo } = this.state;
        filteredInfo = filteredInfo || {};
        const columns = [
            {
                title: 'ID',
                dataIndex: 'key',
                key: 'key',
            },
            {
                title: 'Work Item',
                dataIndex: 'name',
                key: 'name',
            },
            {
                title: 'Due date',
                dataIndex: 'dueDate',
                key: 'dueDate',
            },
            {
                title: 'Number of resources',
                dataIndex: 'numberOfResources',
                key: 'numberOfResources',
            },
            {
                title: 'Status',
                dataIndex: 'status',
                key: 'status',
                // filters: [{ text: 'Over due', value: 'Over due' }, { text: 'Done', value: 'Done' },{ text: 'In Progress', value: 'In progress' }],
                // filteredValue: filteredInfo.name || null,
                // onFilter: (value, record) => record.name.includes(value),
            },
            {
                title: 'Action',
                key: 'action',
                render: (text, record) => (
                    <span>
                        <a onClick={() => {
                            this.handleEdit(record.key)
                        }} href="javascript:;"><Icon type="edit" /></a>
                        <Divider type="vertical" />
                        <a
                            onClick={() => {
                                this.handleDelete(record.key)
                            }}

                            href="javascript:;"><Icon type="delete" /></a>
                    </span>
                ),
            },
        ]
        return (
            <div className="App">
                <div>
                <div style={{marginLeft:'12px',padding:'5px'}}>Number of work items:{this.state.items.length}</div>
                <Button onClick={this.showModal} ><Icon type="file-add" /></Button>
                </div>
                <Table dataSource={this.state.items} columns={columns} />
                <AddItem
                    title="Add Item"
                    visible={this.state.visible}
                    handleOk={this.handleOk}
                    handleCancel={this.handleCancel}
                    name={this.state.name}
                    numberOfResources={this.state.numberOfResources}
                    onHandleName={this.onHandleName}
                    onHandleAge={this.onHandleAge}
                    onHandleDate={this.onHandleDate}
                    onOptionChange={this.onOptionChange}
                />
                <AddItem
                    title="Update Item"
                    visible={this.state.editVisible}
                    handleOk={this.handleEditOk}
                    handleCancel={this.handleEditCancel}
                    name={this.state.name}
                    numberOfResources={this.state.numberOfResources}
                    dueDate={this.state.dueDate}
                    status={this.state.status}
                    onHandleName={this.onHandleName}
                    onHandleAge={this.onHandleAge}
                    onHandleDate={this.onHandleDate}
                    onOptionChange={this.onOptionChange}
                />

            </div>

        );
    }

}

export default App;
