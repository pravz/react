import React from 'react'
import { Modal } from 'antd';
import { Input } from 'antd';
import { InputNumber } from 'antd';
import { DatePicker } from 'antd';
import moment from 'moment';
import { Select } from 'antd';
import Button from 'antd/lib/button';

const { Option } = Select;



const dateFormat = 'YYYY-MM-DD';

class AddItem extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            dob: moment('2015-06-06', dateFormat).format('YYYY-MM-DD')
        }
    }

    render() {
        return (
            <Modal
                title={this.props.title}
                visible={this.props.visible}
                onOk={this.props.handleOk}
                onCancel={this.props.handleCancel}
            // okButtonProps={{ disabled: this.state.isAllset }}
            >
                <Input value={this.props.name} size='large' placeholder="Work item" onChange={this.props.onHandleName} />
                <br />
                <InputNumber value={this.props.numberOfResources} size='large' placeholder="Number of resources" onChange={this.props.onHandleAge} />
                <br />
                {
                    this.props.dob ?
                        <DatePicker defaultValue={moment(this.props.dueDate, dateFormat)} onChange={this.props.onHandleDate} />
                        :
                        <DatePicker onChange={this.props.onHandleDate} />
                }
                <br />
                {
                    this.props.status ?
                        <Select
                            value = {this.props.status}
                            showSearch
                            style={{ width: 200 }}
                            placeholder="Select status"
                            optionFilterProp="children"
                            onChange={this.props.onOptionChange}
                            filterOption={(input, option) =>
                                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                        >
                            <Option value="Over due">Over due</Option>
                            <Option value="Done">Done</Option>
                            <Option value="In progress">In Progress</Option>
                        </Select> :

                        <Select
                            showSearch
                            style={{ width: 200 }}
                            placeholder="Select a person"
                            optionFilterProp="children"
                            onChange={this.props.onOptionChange}
                            filterOption={(input, option) =>
                                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                        >
                            <Option value="Over due">Over due</Option>
                            <Option value="Done">Done</Option>
                            <Option value="In progress">In Progress</Option>
                        </Select>

                }

            </Modal>
        )
    }
}

export default AddItem